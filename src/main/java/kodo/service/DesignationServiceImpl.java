package kodo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import kodo.model.Designation;
import kodo.repository.DesignationRepository;

@Service
public class DesignationServiceImpl implements DesignationService {
	/***
	 * Designation Service to get the designation data based on multiple sorting and searching
	 */

	@Autowired
	private DesignationRepository designationRepository;
	
	@Override
	public List<Designation> getAllDesignation(Integer pageNo, Integer pageSize, String sortBy) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		
		Page<Designation> pagedResult = designationRepository.findAll(paging);
		
		// use when don't need total page count and related information
		//Slice<Designation> slicedResult = designationRepository.findAll(paging);

		if (pagedResult.hasContent()) {
			return pagedResult.getContent();
		} else {
			return new ArrayList<Designation>();
		}
	}
	
	@Override
	public List<Designation> getAllDesignation(String keyword) {
		List<Designation> designationResult = new ArrayList<Designation>();
		if (checkSearchKeyword(keyword)) {
			keyword = keyword.substring(1, keyword.length() - 1);
			designationResult = designationRepository.exactSearch(keyword);
		} else {
			designationResult = designationRepository.search(keyword);
		}
		return designationResult;
	}
	
	private boolean checkSearchKeyword(String keyword) {
		if(keyword.startsWith("\"") && keyword.endsWith("\"")) return true;
		return false;
	}

	@Override
	public List<Designation> getAllDesignation() {
		List<Designation> allDesignation = designationRepository.findAll();
		return allDesignation;
	
	}
}

