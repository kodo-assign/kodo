package kodo.service;

import java.util.List;

import kodo.model.Designation;

public interface DesignationService {
	
	public List<Designation> getAllDesignation();
	public List<Designation> getAllDesignation(Integer pageNo, Integer pageSize, String sortBy);
	public List<Designation> getAllDesignation(String keyword);
}
