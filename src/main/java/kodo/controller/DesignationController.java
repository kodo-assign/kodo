package kodo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kodo.model.Designation;
import kodo.service.DesignationService;

 
@RestController
@RequestMapping("/api")
public class DesignationController
{
    @Autowired
    private DesignationService designationService;
 
    @GetMapping("/designation")
    public ResponseEntity<List<Designation>> getAllDesignation(
                        @RequestParam(defaultValue = "0") Integer pageNo,
                        @RequestParam(defaultValue = "10") Integer pageSize,
                        @RequestParam(defaultValue = "title") String sortBy)
    {
        List<Designation> list = designationService.getAllDesignation(pageNo, pageSize, sortBy);
 
        return new ResponseEntity<List<Designation>>(list, new HttpHeaders(), HttpStatus.OK);
    }
    
    @GetMapping("/designation/search")
    public ResponseEntity<List<Designation>> getAllDesignation(
                        @RequestParam String searchString)
    {
        List<Designation> list = designationService.getAllDesignation(searchString);
 
        return new ResponseEntity<List<Designation>>(list, new HttpHeaders(), HttpStatus.OK);
    }

 
}