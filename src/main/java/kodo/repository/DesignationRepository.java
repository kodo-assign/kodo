package kodo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kodo.model.Designation;

 
public interface DesignationRepository extends JpaRepository<Designation, Long> {
		
	@Query(value = "SELECT * FROM designation d WHERE MATCH(d.title,d.description) AGAINST(?1)", nativeQuery = true)
	public List<Designation> search(String keyword);
	
	@Query("SELECT d FROM Designation d WHERE concat(d.title, d.description) LIKE CONCAT('%',:keyword,'%')")
	public List<Designation> exactSearch(@Param("keyword") String keyword);
	
//	@Query(value = "SELECT * FROM designation t WHERE " + "t.title LIKE CONCAT('%',?1,'%') OR "
//			+ "t.description LIKE CONCAT('%',?1,'%')", nativeQuery = true)
//	public List<Designation> exactSearch(String searchTerm);
	
	
}