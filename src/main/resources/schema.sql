DROP TABLE IF EXISTS designation;
 
CREATE TABLE designation (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  title VARCHAR(250) NOT NULL,
  image VARCHAR(250) NOT NULL,
  description VARCHAR(550) DEFAULT NULL,
  last_edited_date VARCHAR(250) NOT NULL
);

ALTER TABLE designation ADD FULLTEXT(title, description);

