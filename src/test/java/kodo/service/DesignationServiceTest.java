package kodo.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import kodo.model.Designation;
import kodo.repository.DesignationRepository;


@RunWith(MockitoJUnitRunner.class)
public class DesignationServiceTest {

	@Mock
	private DesignationRepository designationRepository;
	
	@InjectMocks
	private DesignationServiceImpl designationService;
	
	@Test
	public void testGetAllDesignationBasedOnPageNoAndSorting() {
		
		int pageNumber = 0;
        int pageSize = 1;
        String sorting = "title";
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(sorting));
        
        Designation designation1 = new Designation(1l, "title 1", "image 1", "description 1", "2018-05-19T12:33:25.545Z");
        
        Page<Designation> pagedResult = new PageImpl<Designation>(Arrays.asList(designation1));
        
        when(designationRepository.findAll(pageable)).thenReturn(pagedResult);

		
		List<Designation> designations = designationService.getAllDesignation(0, 1, "title");
		assertEquals(designations.size(), 1);
	}
	
	@Test
	public void testGetAllDesignationBasedOnSearchQuery() {
		String searchQuery = "title description";
		
		designationService.getAllDesignation(searchQuery);
		verify(designationRepository).search(searchQuery);
	}
	
	@Test
	public void testGetAllDesignationBasedOnExactSearchQuery() {
		String searchQuery = "\"title description\"";
		String searchQueryToDb = "title description";
		designationService.getAllDesignation(searchQuery);
		verify(designationRepository).exactSearch(searchQueryToDb);
	}
}


