package kodo.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import kodo.model.Designation;
import kodo.service.DesignationServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class DesignationControllerIntegrationTest {

	@Mock
	private DesignationServiceImpl designationService;
	
	@InjectMocks
    private DesignationController designationController;
	
	
	private MockMvc mockMvc;
	
	
	@org.junit.Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(designationController).build();
	}
	
	
	@Test
	public void shouldFetchAllDesignationBasedOnPageNoAndSorting() throws Exception {

		int pageNo = 0;
		int pageSize = 10;
		String sortBy = "title";
		Designation designation = new Designation(1l, "title 1", "image 1", "description 1",
				"2018-05-19T12:33:25.545Z");
		
        ObjectMapper objectMapper = new ObjectMapper();		
        Map resultAsJson = objectMapper.readValue(objectMapper.writeValueAsString(designation), Map.class);


		when(designationService.getAllDesignation(pageNo, pageSize, sortBy)).thenReturn(Arrays.asList(designation));

		mockMvc.perform(get("/api/designation?pageNo=0&pageSize=10&sortBy=title"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").value(resultAsJson));

	}
	
	@Test
	public void shouldFetchAllDesignationBasedOnSearchQuery() throws Exception {

		String searchString = "title";
		Designation designation1 = new Designation(1l, "title 1", "image 1", "description 1",
				"2018-05-19T12:33:25.545Z");

		
        ObjectMapper objectMapper = new ObjectMapper();		
        Map resultAsJson = objectMapper.readValue(objectMapper.writeValueAsString(designation1), Map.class);


		when(designationService.getAllDesignation(searchString)).thenReturn(Arrays.asList(designation1));

		mockMvc.perform(get("/api/designation/search?searchString=title"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").value(resultAsJson));

	}
}
