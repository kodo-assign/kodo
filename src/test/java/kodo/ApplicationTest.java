package kodo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Profile;




@SpringBootApplication
@Profile("test")
public class ApplicationTest {

	public static void main(String[] args) throws Exception {
		ConfigurableApplicationContext context = SpringApplication.run(ApplicationTest.class, args);
		// context.getBean(ActivityVariantService.class).updateDefaultVariants();
	}
}
