# README #

### Kodo Searching, Sorting and Pagination Project ###

* This project contains basic searching, sorting and pagination feature
* 1.0

### SetUp ###

* Dependencies : 
	To install the dependencies run "mvn clean install"

	a) This will create the build and run all test cases

* Database configuration:
    To setup database use mysql database
    a) create table kodo

    other schema will automatically create on project startup

* How to run tests :
	To run the test cases run "mvn clean test"

* How to start project :
	mvn spring-boot:run
